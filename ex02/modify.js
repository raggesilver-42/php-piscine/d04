const router    = require('express').Router();
const reqparams = require('../modules/reqparams').reqparams;
const notEmpty  = require('../modules/reqparams').notEmpty;
const Session   = require('../modules/session');
const pug       = require('pug');
const path      = require('path');

router.get('/', (req, res) => {
    res.send(pug.compileFile(path.join(__dirname, 'index.pug'))());
});

const modifPostParams = {
    login:  { validate: notEmpty, msg: 'ERROR\n' },
    oldpw:  { validate: notEmpty, msg: 'ERROR\n' },
    newpw:  { validate: notEmpty, msg: 'ERROR\n' },
    submit: { validate: (val) => val == 'OK', msg: 'ERROR\n' },
};

router.post('/modif', reqparams(modifPostParams), (req, res) => {
    // Get the hashed password
    let hpass = Session._database.get(req.body.login);

    if (!hpass || !Session._database.compare(req.body.oldpw, hpass))
        return res.status(400).end('ERROR\n');

    Session._database.set(req.body.login,
                          Session._database.hash(req.body.newpw));

    return res.end('OK\n');
});

module.exports = router;
