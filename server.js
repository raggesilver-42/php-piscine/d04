// const mongoose    = require('mongoose');
const express     = require('express');
const app         = express();
const bodyPaser   = require('body-parser');
const cookiePaser = require('cookie-parser');
const router      = require('./router');

app.use(bodyPaser.urlencoded({ extended: false }));
app.use(bodyPaser.json());
app.use(cookiePaser());

router(app);

// const mongoOpts = { useNewUrlParser: true, useUnifiedTopology: true };
// mongoose.connect(process.env.DBURL, mongoOpts)
//     .then(() => console.log('Mongoose connected'))
//     .catch(err => console.log('DB connection error ', err));

app.listen(process.env.PORT || 3000, function () {
    console.log(`App listening on port ${this.address().port}`);
});
