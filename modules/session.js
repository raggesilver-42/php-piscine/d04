const crypto = require('crypto');
const bcrypt = require('bcrypt');
const path   = require('path');
const fs     = require('fs');

function mkdirp(path)
{
    if (!fs.existsSync(path))
        fs.mkdirSync(path, { recursive: true });
}

class DbPriv
{
    constructor(path)
    {
        this.path = path;
    }

    getData()
    {
        if (!fs.existsSync(this.path))
            return ({});

        try
        {
            let data = fs.readFileSync(this.path).toString();

            if (!data || data.trim() == '')
                return ({});

            data = JSON.parse(data);
            return (data);
        }
        catch(e)
        {
            throw e;
        }
    }

    setData(key, value)
    {
        let data = this.getData();

        data[key] = value;

        this.saveData(data);
    }

    saveData(data)
    {
        fs.writeFileSync(this.path, JSON.stringify(data));
    }
}

const pd = new WeakMap();
class PauloDb
{
    /**
     * Initialize a new PauloDb
     * @param {String} file_path the name of the file to store the data
     */
    constructor(file_path)
    {
        this.path = file_path;
        // Ensure the path to the file exists
        mkdirp(path.dirname(file_path));

        // Set the private class
        pd.set(this, {
            priv: new DbPriv(file_path)
        });
    }

    compare(plain, hashed)  { return (bcrypt.compareSync(plain, hashed)); }

    hash(data)              { return (bcrypt.hashSync(data, 10)); }

    get(key)                { return (pd.get(this).priv.getData()[key]); }

    set(key, val)           { pd.get(this).priv.setData(key, val); }
}

const ss = new WeakMap();
class Session
{
    constructor()
    {
        this.sess = {};

        ss.set(this, {
            db: new PauloDb(path.join(__dirname, '..', 'private', 'passwd'))
        });
    }

    randomHash()
    {
        let hash = null;

        do {
            hash = crypto.randomBytes(20).toString('hex');
        } while (hash in this.sess);

        return (hash);
    }

    /**
     * @returns {PauloDb}
     */
    get _database() { return (ss.get(this).db); }

    startSession(req, res)
    {
        let sessid = req.cookies.sess || null;

        if (!sessid || !(sessid in this.sess))
        {
            sessid = this.randomHash();
            res.cookie('sess', sessid, { maxAge: 900000, httpOnly: true });
            this.sess[sessid] = {};
        }

        req.session = this.sess[sessid];
    }

    endSession(req, res)
    {
        if (req.cookies.sess)
        {
            delete this.sess[req.cookies.sess];
            res.clearCookie('sess');
        }
    }

    /**
     * Validate a username + password combo.
     *
     * # auth function for ex03
     *
     * @param {String} username
     * @param {String} pass password
     *
     * @returns {Boolean} Whether the combo is a valid login or not.
     */
    auth(username, pass)
    {
        let savedPass = this._database.get(username);

        if (!savedPass || !this._database.compare(pass, savedPass))
            return (false);

        return (true);
    }
}

const s = new Session();

module.exports = s;
