module.exports = {
    /**
     * @param {Object} params Object with required keys
     */
    reqparams(params) {
        return (req, res, next) => {
            for (let k in params) {
                if (!(k in req.body) && !params[k].optional)
                    return res.status(400).end(
                        params[k].msg || `Param '${k}' missing.`
                    );
                if (params[k].validate && !params[k].validate(req.body[k]))
                    return res.status(400).end(
                        params[k].msg || `Invalid param '${k}'.`
                    );
            }
            next();
        };
    },

    /**
     * @param {Object} params Object with required keys
     */
    reqquery(params) {
        return (req, res, next) => {
            for (let k in params) {
                if (!(k in req.query) && !params[k].optional)
                    return res.status(400).end(
                        params[k].msg || `Param '${k}' missing.`
                    );
                if (params[k].validate && !params[k].validate(req.query[k]))
                    return res.status(400).end(
                        params[k].msg || `Invalid param '${k}'.`
                    );
            }
            next();
        };
    },

    /**
     * Check if a string only contains whitespaces
     * @param {String} str The string to be tested
     *
     * @returns {Boolean}
     */
    notEmpty(str) {
        return (!/^\s*$/.test(str));
    }

};
