module.exports = (app) => {

    const routes = [
        { url: '/ex00', path: './ex00/index.js' },
        { url: '/ex01', path: './ex01/create.js' },
        { url: '/ex02', path: './ex02/modify.js' },
        { url: '/ex03', path: './ex03/auth.js' },
    ];

    for (const route of routes) {
        app.use(route.url, require(route.path));
    }

};
