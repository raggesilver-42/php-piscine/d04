const router       = require('express').Router();
const Session      = require('../modules/session');
const { reqquery } = require('../modules/reqparams');

const loginGetParams = {
    login:  { msg: 'ERROR\n' },
    passwd: { msg: 'ERROR\n' }
};

router.get('/login', reqquery(loginGetParams), (req, res) => {
    if (!Session.auth(req.query.login, req.query.passwd))
        return res.status(401).end('ERROR\n');

    Session.startSession(req, res);
    req.session.login = req.query.login;

    res.end('OK\n');
});

router.get('/whoami', (req, res) => {
    if (!req.cookies.sess || !(req.cookies.sess in Session.sess))
        return res.status(401).end('ERROR\n');

    Session.startSession(req, res);

    res.end(req.session.login);
});

router.get('/logout', (req, res) => {
    Session.endSession(req, res);
    res.end();
});

module.exports = router;
