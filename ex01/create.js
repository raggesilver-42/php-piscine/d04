const router    = require('express').Router();
const pug       = require('pug');
const path      = require('path');
const reqparams = require('../modules/reqparams').reqparams;
const notEmpty  = require('../modules/reqparams').notEmpty;
const Session   = require('../modules/session');

router.get('/', (req, res) => {
    res.send(pug.compileFile(path.join(__dirname, 'index.pug'))());
});


const createPostParams = {
    login:  { validate: notEmpty, msg: 'ERROR\n' },
    passwd: { validate: notEmpty, msg: 'ERROR\n' },
    submit: { validate: (val) => val == 'OK', msg: 'ERROR\n' },
};

router.post('/create', reqparams(createPostParams), (req, res) => {
    // Check if user is already registered in the database
    if (Session._database.get(req.body.login))
        return res.status(400).end('ERROR\n');

    Session._database.set(req.body.login,
                          Session._database.hash(req.body.passwd));

    res.end('OK\n');
});

module.exports = router;
