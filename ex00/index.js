const router = require('express').Router();
const pug    = require('pug');
const path   = require('path');
const crypto = require('crypto');

let sessions = {};

router.get('/', (req, res) => {

    let login  = req.query.login || '';
    let passwd = req.query.passwd || '';
    let submit = req.query.submit || false;

    if (login && passwd && submit === "OK")
    {
        let sessid = req.cookies.sess || crypto.randomBytes(20).toString('hex');

        res.cookie('sess', sessid, {
            maxAge: 900000, httpOnly: true
        });

        sessions[sessid] = { login, passwd };
    }
    else if (req.cookies.sess && req.cookies.sess in sessions)
    {
        login  = sessions[req.cookies.sess].login;
        passwd = sessions[req.cookies.sess].passwd;
    }

    let locals   = { login, passwd };
    let filePath = path.join(__dirname, 'index.pug');

    res.send(
        pug.compileFile(filePath)(locals)
    );
});

module.exports = router;
